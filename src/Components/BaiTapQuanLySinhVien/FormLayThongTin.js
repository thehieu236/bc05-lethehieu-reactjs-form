import React, { Component } from "react";
import { Button } from "react-bootstrap";

import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import { connect } from "react-redux";

class FormLayThongTin extends Component {
  state = {
    values: {
      maSV: "",
      hoVaTen: "",
      soDienThoai: "",
      email: "",
    },
    errors: {
      maSV: "",
      hoVaTen: "",
      soDienThoai: "",
      email: "",
    },
    valid: false,
  };

  handleChange = (event) => {
    const { name, value, type, pattern } = event.target;

    let errorMessage = "";

    // Kiểm tra rỗng
    if (value.trim() === "") {
      errorMessage = name + "không được bỏ rổng!";
    }

    const regex = new RegExp(pattern);

    if (type === "email") {
      if (!regex.test(value)) {
        errorMessage = name + " không đúng định dạng.";
      }
    }

    if (name === "soDienThoai") {
      if (!regex.test(value)) {
        errorMessage = name + " không đúng định dạng.";
      }
    }

    if (name === "maSV") {
      if (!regex.test(value)) {
        errorMessage = name + " phải là số.";
      }
    }

    if (name === "soDienThoai") {
      if (!regex.test(value)) {
        errorMessage = name + " không đúng định dạng.";
      }
    }

    if (name === "hoVaTen") {
      if (!regex.test(value)) {
        errorMessage = name + " không đúng định dạng.";
      }
    }

    let values = { ...this.state.values, [name]: value };
    let errors = { ...this.state.errors, [name]: errorMessage };
    this.setState(
      {
        ...this.state,
        values: values,
        errors: errors,
      },
      () => {
        this.checkValid();
      }
    );
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.themSinhVien(this.state);
    this.setState({
      values: {
        maSV: "",
        hoVaTen: "",
        soDienThoai: "",
        email: "",
      },
      errors: {
        maSV: "",
        hoVaTen: "",
        soDienThoai: "",
        email: "",
      },
      valid: false,
    });
  };

  checkValid = () => {
    let valid = true;
    for (let key in this.state.errors) {
      if (this.state.errors[key] !== "" || this.state.values[key] === "") {
        valid = false;
      }
    }

    this.setState({
      ...this.state,
      valid: valid,
    });

    return (
      <Button variant="primary" type="submit" disabled>
        Thêm sinh viên
      </Button>
    );
  };

  render() {
    return (
      <Form onSubmit={this.handleSubmit}>
        <h2 className="bg-info text-white p-3">Thông tin sinh viên</h2>
        <Row className="mb-3">
          <Form.Group as={Col} controlId="formGridEmail">
            <Form.Label>Mã SV</Form.Label>

            <Form.Control
              name={"maSV"}
              value={this.state.values.maSV}
              onChange={this.handleChange}
              type="text"
              pattern="^[0-9]*$"
              placeholder="Nhập mã sinh viên"
            />
            <Form.Text className="text-danger">
              {this.state.errors.maSV}
            </Form.Text>
          </Form.Group>

          <Form.Group as={Col} controlId="formGridPassword">
            <Form.Label>Họ và tên</Form.Label>

            <Form.Control
              name={"hoVaTen"}
              value={this.state.values.hoVaTen}
              onChange={this.handleChange}
              type="text"
              pattern="^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$"
              placeholder="Nhập họ và tên"
            />
            <Form.Text className="text-danger">
              {this.state.errors.hoVaTen}
            </Form.Text>
          </Form.Group>
        </Row>
        <Row className="mb-3">
          <Form.Group as={Col} controlId="formGridEmail">
            <Form.Label>Số điện thoại</Form.Label>

            <Form.Control
              name={"soDienThoai"}
              value={this.state.values.soDienThoai}
              onChange={this.handleChange}
              type="text"
              placeholder="Nhập số điện thoại"
              pattern="^[0-9]*$"
            />
            <Form.Text className="text-danger">
              {this.state.errors.soDienThoai}
            </Form.Text>
          </Form.Group>

          <Form.Group as={Col} controlId="formGridPassword">
            <Form.Label>Email</Form.Label>
            <Form.Control
              name={"email"}
              value={this.state.values.email}
              onChange={this.handleChange}
              type="email"
              placeholder="Nhập địa chỉ email"
              pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
            />
            <Form.Text className="text-danger">
              {this.state.errors.email}
            </Form.Text>
          </Form.Group>
        </Row>

        {this.state.valid ? (
          <Button variant="primary" type="submit">
            Thêm sinh viên
          </Button>
        ) : (
          <Button variant="dark" type="submit" disabled>
            Thêm sinh viên
          </Button>
        )}
      </Form>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    themSinhVien: (sinhVien) => {
      const action = {
        type: "THEM_SINH_VIEN",
        sinhVien,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(FormLayThongTin);
