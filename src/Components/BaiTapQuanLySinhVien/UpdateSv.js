import React, { Component } from "react";
import { Button, Col, Form, Row } from "react-bootstrap";
import { connect } from "react-redux";

class UpdateSv extends Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.capNhatSinhVien(this.state);
  };

  render() {
    const { mangUpdateSv } = this.props;
    return (
      <Form onSubmit={this.handleSubmit}>
        <Row className="mb-3">
          <Form.Group as={Col} controlId="formGridEmail">
            <Form.Label>Mã SV</Form.Label>

            <Form.Control
              name={"maSV"}
              onChange={this.handleChange}
              type="text"
              pattern="^(0|[1-9][0-9]*)$"
              value={mangUpdateSv[0].values.maSV}
              placeholder="Nhập mã sinh viên"
            />
            {/* <Form.Text className="text-danger">
              value={mangUpdateSv[0].errors.maSV}
            </Form.Text> */}
          </Form.Group>

          <Form.Group as={Col} controlId="formGridPassword">
            <Form.Label>Họ và tên</Form.Label>

            <Form.Control
              name={"hoVaTen"}
              onChange={this.handleChange}
              type="text"
              pattern="^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$"
              value={mangUpdateSv[0].values.hoVaTen}
              placeholder="Nhập họ và tên"
            />
            {/* <Form.Text className="text-danger">
              value={mangUpdateSv[0].errors.hoVaTen}
            </Form.Text> */}
          </Form.Group>
        </Row>
        <Row className="mb-3">
          <Form.Group as={Col} controlId="formGridEmail">
            <Form.Label>Số điện thoại</Form.Label>

            <Form.Control
              name={"soDienThoai"}
              onChange={this.handleChange}
              type="text"
              placeholder="Nhập số điện thoại"
              pattern="^(0|[1-9][0-9]*)$"
              value={mangUpdateSv[0].values.soDienThoai}
            />
            {/* <Form.Text className="text-danger">
              value={mangUpdateSv[0].errors.soDienThoai}
            </Form.Text> */}
          </Form.Group>

          <Form.Group as={Col} controlId="formGridPassword">
            <Form.Label>Email</Form.Label>
            <Form.Control
              name={"email"}
              onChange={this.handleChange}
              type="email"
              placeholder="Nhập địa chỉ email"
              pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
              value={mangUpdateSv[0].values.email}
            />
            {/* <Form.Text className="text-danger">
              value={mangUpdateSv[0].errors.email}
            </Form.Text> */}
          </Form.Group>
        </Row>

        <Button variant="primary" type="submit">
          Cập nhật
        </Button>
        <Button variant="dark" type="submit" disabled>
          Cập nhật
        </Button>
      </Form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    mangUpdateSv: state.QuanLySinhVienReducer.mangUpdateSv,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    capNhatSinhVien: (index) => {
      const action = {
        type: "CHINH_SUA_SINH_VIEN",
        index,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateSv);
