import React, { Component } from "react";
import { connect } from "react-redux";

class Search extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchText: "",
    };
  }

  isChange = (e) => {
    this.setState({
      searchText: e.target.value,
    });
  };

  render() {
    return (
      <div class="d-flex mt-5">
        <input
          onChange={(e) => {
            this.isChange(e);
          }}
          type="text"
          class="form-control"
          placeholder="Nhập thông tin sinh viên,..."
        />
        <button
          style={{ width: "150px" }}
          className="btn btn-info ml-2"
          onClick={() => {
            this.props.timSinhVien(this.state.searchText);
          }}
        >
          Tìm
        </button>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    timSinhVien: (searchText) => {
      const action = {
        type: "TIM_KIEM_SINH_VIEN",
        payload: searchText,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(Search);
