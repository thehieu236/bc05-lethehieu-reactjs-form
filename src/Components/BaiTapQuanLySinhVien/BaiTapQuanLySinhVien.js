import React, { PureComponent } from "react";
import FormLayThongTin from "./FormLayThongTin";
import TableDanhSachSinhVien from "./TableDanhSachSinhVien";
import Search from "./Search";

class BaiTapQuanLySinhVien extends PureComponent {
  render() {
    return (
      <div className="container">
        <FormLayThongTin />
        <Search />
        <TableDanhSachSinhVien />
      </div>
    );
  }
}

export default BaiTapQuanLySinhVien;
