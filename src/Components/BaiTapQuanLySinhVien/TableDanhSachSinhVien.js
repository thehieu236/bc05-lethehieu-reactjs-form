import React, { Component } from "react";
import { Button } from "react-bootstrap";
import Table from "react-bootstrap/Table";
import { connect } from "react-redux";
import ModalShow from "./ModalShow";

class TableDanhSachSinhVien extends Component {
  renderSinhVien = (arrSv) => {
    return arrSv.map((sinhVien, index) => {
      return (
        <tr key={index}>
          <td>{sinhVien.values.maSV}</td>
          <td>{sinhVien.values.hoVaTen}</td>
          <td>{sinhVien.values.soDienThoai}</td>
          <td>{sinhVien.values.email}</td>
          <td className="d-flex">
            <Button
              className="mr-2"
              variant="danger"
              type="submit"
              onClick={() => {
                this.props.xoaSinhVien(index);
              }}
            >
              Xóa
            </Button>

            <div
              onClick={() => {
                this.props.updateSinhVien(index);
              }}
            >
              <ModalShow />
            </div>
          </td>
        </tr>
      );
    });
  };

  render() {
    const { mangSV } = this.props;
    return (
      <div className="App">
        <Table striped bordered hover>
          <thead>
            <tr className="bg-dark text-white p-3">
              <th style={{ width: 100 }}>Mã SV</th>
              <th>Họ và tên</th>
              <th>Số điện thoại</th>
              <th>Email</th>
              <th style={{ width: 200 }}>Hành động</th>
            </tr>
          </thead>
          <tbody>{this.renderSinhVien(mangSV)}</tbody>
        </Table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    mangSV: state.QuanLySinhVienReducer.mangSV,
    mangUpdateSv: state.QuanLySinhVienReducer.mangUpdateSv,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    xoaSinhVien: (index) => {
      const action = {
        type: "XOA_SINH_VIEN",
        payload: index,
      };
      dispatch(action);
    },
    updateSinhVien: (index) => {
      const action = {
        type: "CHINH_SUA_SINH_VIEN",
        payload: index,
      };
      dispatch(action);
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TableDanhSachSinhVien);
