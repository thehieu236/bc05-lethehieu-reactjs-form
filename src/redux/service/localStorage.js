export const THONG_TIN_SINH_VIEN = "THONG_TIN_SINH_VIEN";

export const svLocalService = {
  set: (svData) => {
    let svJson = JSON.stringify(svData);
    localStorage.setItem(THONG_TIN_SINH_VIEN, svJson);
  },
  get: () => {
    let svJSon = localStorage.getItem(THONG_TIN_SINH_VIEN);

    if (svJSon != null) {
      return JSON.parse(svJSon);
    } else {
      return null;
    }
  },
  remove: () => {
    localStorage.removeItem(THONG_TIN_SINH_VIEN);
  },
};
