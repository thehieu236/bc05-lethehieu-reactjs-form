import { combineReducers } from 'redux';
import { QuanLySinhVienReducer } from './BaiTapQuanLySinhVienReducer';

export const rootReducer = combineReducers({
	QuanLySinhVienReducer,
});
