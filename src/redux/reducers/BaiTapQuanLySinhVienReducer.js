import {
  CHINH_SUA_SINH_VIEN,
  THEM_SINH_VIEN,
  TIM_KIEM_SINH_VIEN,
  XOA_SINH_VIEN,
} from "../constants/ActionTypes";

const stateDefaul = {
  mangSV: [],
  mangUpdateSv: [],
};

export const QuanLySinhVienReducer = (state = stateDefaul, action) => {
  switch (action.type) {
    case TIM_KIEM_SINH_VIEN:
      return {
        ...state,
        mangSV: state.mangSV.filter(
          (item, index) =>
            item.values.maSV === action.payload ||
            item.values.hoVaTen === action.payload ||
            item.values.soDienThoai === action.payload ||
            item.values.email === action.payload
        ),
      };

    case THEM_SINH_VIEN:
      let maSVUpdate = [...state.mangSV, action.sinhVien];
      state.mangSV = maSVUpdate;
      return { ...state };

    case CHINH_SUA_SINH_VIEN:
      return {
        ...state,
        mangUpdateSv: state.mangSV.filter(
          (item, index) => index === action.payload
        ),
      };

    case XOA_SINH_VIEN:
      return {
        ...state,
        mangSV: state.mangSV.filter((item, index) => index !== action.payload),
      };

    default:
      return { ...state };
  }
};
